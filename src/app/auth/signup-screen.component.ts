import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { User } from "./user.model";

@Component({
    selector: 'app-signup-screen',
    templateUrl: './signup-screen.component.html'
})

export class SignupScreenComponent implements OnInit{
    signinForm: FormGroup;

    ngOnInit() {
        this.signinForm = new FormGroup({
            name: new FormControl(null, [Validators.required]),
            lastName: new FormControl(null, [Validators.required]),
            email: new FormControl(null, [
                Validators.required,
                Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ]),
            password: new FormControl(null, Validators.required)
        });
    }

    onSubmit(){
        if(this.signinForm.valid) {
            const {email, password, name, lastName} = this.signinForm.value;
            const user = new User(email, password, name, lastName);
            console.log(user);
        }
    }
}