import { Injectable } from "@angular/core";
import { Question } from "./question.model";
import { Http } from '@angular/http'
import { environment } from "../../environments/environment";
import * as urlJoin from 'url-join';
import 'rxjs/add/operator/toPromise'
import { HttpClient } from "@angular/common/http";

@Injectable()
export class QuestionService {
    private questionsUrl: string;

    constructor(private http: HttpClient){
        this.questionsUrl = urlJoin(environment.apiUrl);
    }


    getQuestions(): Promise<any> {
        return this.http.get('http://localHost:3000/')
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(this.handleError);
    }

    // getQuestion(id): Promise<void | Question[]> {
    //     const url = urlJoin(this.questionsUrl, id)
    //     return this.http.get(url)
    //         .toPromise()
    //         .then(response => response.json() as Question)
    //         .catch(this.handleError);
    // }

    handleError(error: any): any {
        const errMsg = error.message ? error.message :
        error.status ? `${error.status} - ${error.status.text}` : `Server error`;
        console.log(errMsg); 
    }
}