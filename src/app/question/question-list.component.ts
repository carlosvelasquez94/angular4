import { Component, OnInit } from '@angular/core';
import { Question } from './question.model';
import { QuestionService } from './question.service';
import { HttpClient } from "@angular/common/http";

@Component({
    selector: 'app-question-list',
    templateUrl: './question-list.component.html',
    providers: [QuestionService]
})

export class QuestionListComponent implements OnInit{
    constructor(private questionService: QuestionService, private http: HttpClient){

    }
    questions: Question[];
    loading = true;

    ngOnInit() {
        this.http.get('http://localHost:5000/').subscribe(data => {
            console.log(data);
          });
    }
}