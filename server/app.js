import express from 'express';
import { question } from './routes'

const app = express();

console.log(`NODE_ENV=${process.env.NODE_ENV}`);
if (process.env.NODE_ENV === 'development') {
    console.log('entre')
    app.use((req, res, next) => {
        res.setHeader('Acces-Control-Allow-Origin', 'http://localhost:4200')
        res.setHeader('Acces-Control-Allow-Headers', 'Origin, X-Request-Whith, Content-Type, Accept')
        res.setHeader('Acces-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS')
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    })
}

app.use('/api/question', question);

export default app;
