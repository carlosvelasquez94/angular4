import express from 'express'
const app = express.Router()

const question = {
    _id: 1,
    title: 'test',
    description: 'test',
    createdAt: new Date(),
    icon: 'devicon-android-plain',
    answers: [],
    user: {
        firstName: 'carlos',
        lastNAme: 'velasquez',
        email: 'carlos@gmail.com',
        password: '123456'
    }
}

const questions = new Array(10).fill(question);

app.get('/', (req, res) => res.status(200).json(questions))
app.get('/:id', (req, res) => res.status(200).json(question))

export default app;
